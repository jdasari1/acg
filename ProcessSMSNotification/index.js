const { PubSub } = require('@google-cloud/pubsub');
const pubsub_publish = async (publisher, data) => {
    const dataBuffer = Buffer.from(JSON.stringify(data));
    try {
        await publisher.publishMessage({ data: dataBuffer })
    } catch (error) {
        console.error(`Received error while publishing: ############################ ${error.message}`);
        process.exitCode = 1;
    }
}
exports.ProcessSMSNotification = async (event, context) => {
    let messageCount = 0;
    const timeout = 60;
    const maxMessages = 10;
    const maxWaitTime = 10;
    const pubsubClient = new PubSub();
    const subscription = pubsubClient.subscription(process.env.SMS_SUBSCRIPTIONID);
    const messageHandler = message => {
        messageCount += 1;
        message.ack();
    };
    subscription.on(`message`, messageHandler);
    setTimeout(() => {
        subscription.removeListener('message', messageHandler);
    }, timeout * 1000);
    const message = event.data ? Buffer.from(event.data, 'base64').toString() : 'SMSNOTIFICATION NOT SUBSCRIBED';
    // console.log("Buckets DATA : ", message);
    let outPut = JSON.parse(message);
    console.log("outPut:-------------->", outPut);
    //##################SEND SMS THROUGH TWILO#################################
    // console.log("#################Enter into the twilo###################");
    const twilio = require('twilio');
    try {

        const client = new twilio(process.env.TWILIO_ACCOUNT_SID, process.env.TWILIO_AUTH_TOKEN);

        client.messages.create({
            body: 'Message Send:----->',
            to: '+919703585391',
            from: '+13163658271'
        })
            .then((response) => console.log("Message send through this Id:///////////////:-->", response.sid));

    } catch {
        console.log("error:------->", error)
    }

};
