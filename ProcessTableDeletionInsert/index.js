const { BigQuery } = require('@google-cloud/bigquery');
exports.ProcessTableDeletionInsert = async (event, context) => {
    try {
        const bigquery = new BigQuery();
        const message = event.data ? Buffer.from(event.data, 'base64').toString() : 'DATA NOT RECIEVEED';
        let tableDeletionData = JSON.parse(message);
        let tableDeletionInsertionData = []
        tableDeletionInsertionData.push(tableDeletionData)
        // Insert data into a table
        await bigquery
            .dataset("MetricsExtraction")
            .table("DeletionEvents")
            .insert(tableDeletionInsertionData);
        console.log(`Inserted ${tableDeletionInsertionData.length} rows---->`);
    } catch (err) {
        console.log("error:---->", err)
    }
};