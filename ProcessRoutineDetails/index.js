const { GoogleAuth } = require('google-auth-library');
var { BigQuery } = require('@google-cloud/bigquery');
async function oauthVerification(url, method) {
  try {
    const auth = new GoogleAuth({
      scopes: process.env.API_AUTHSCOPE
    });
    const client = await auth.getClient();
    const response = await client.request({ url, method });
    return response;
  } catch (error) {
    console.log("ERROR : --------------------> ", error)
    throw error;
  }
}
function waitFor(milliseconds) {
  return new Promise((resolve) => setTimeout(resolve, milliseconds));
}
async function retryWithBackoff(promise, onRetry, maxRetries, retries = 0) {
  try {
    if (retries > 0) {
      const timeToWait = 2 ** retries * 100;
      console.log(`waiting for ${timeToWait}ms...`);
      await waitFor(timeToWait);
    }
    return await promise();
  } catch (e) {
    if (retries < maxRetries) {
      onRetry();
      return retryWithBackoff(promise, onRetry, maxRetries, retries + 1);
    } else {
      console.warn("Max retries reached. Bubbling the error up");
      throw e;
    }
  }
}
exports.ProcessRoutineDetails = async (event, context) => {
  try {
    const bigquery = new BigQuery();
    let datasetRoutineListData = [];
    const message = event.data
      ? Buffer.from(event.data, 'base64').toString()
      : 'GETTING DATASET ROUTINE DETAILS';
    let routinesResponse = JSON.parse(message);
    console.log("routinesResponse:---------->", routinesResponse);
    let url = `${process.env.Google_BaseURL}/projects/${routinesResponse.ProjectId}/datasets/${routinesResponse.DatasetId}/routines/${routinesResponse.RoutineId}`;
    console.log("url:-------------------->", url);
    const response = await retryWithBackoff(
      () => oauthVerification(url),
      () => console.log("onRetry called..."),
      3
    );
    let arguments = ''
    arguments = JSON.stringify(response.data.arguments);
    console.log("arguments:------->", arguments)
    let datasetRoutineList = {
      RunId: routinesResponse.RunId,
      ProjectId: routinesResponse.ProjectId,
      DatasetId: routinesResponse.DatasetId,
      RoutineId: routinesResponse.RoutineId,
      RoutineType: routinesResponse.RoutineType,
      CreationTimestamp: routinesResponse.CreationTimestamp,
      LastModifiedTimestamp: routinesResponse.LastModifiedTimestamp,
      RoutineBody: response.data.definitionBody,
      RoutineArgs: arguments
    }
    datasetRoutineListData.push(datasetRoutineList)
    console.log("datasetRoutineListData:------------>", datasetRoutineListData);
    await bigquery
      .dataset("MetricsExtraction")
      .table("DatasetRoutines")
      .insert(datasetRoutineListData);
    console.log(`Inserted ${datasetRoutineListData.length} rows`);
  } catch (err) {
    console.log("error:------------>", err)
  }
};
