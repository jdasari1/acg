var { BigQuery } = require('@google-cloud/bigquery');
var { GoogleAuth } = require('google-auth-library');
async function dateTimeFormat(datevalue) {
  return datevalue.getFullYear() + "-" + (datevalue.getMonth() + 1 > 9 ? datevalue.getMonth() + 1 : "0" + (datevalue.getMonth() + 1)) + "-" + ((datevalue.getDate() > 9 ? datevalue.getDate() : "0" + datevalue.getDate())) + " " + (datevalue.getHours() > 9 ? datevalue.getHours() : "0" + (datevalue.getHours())) + ":" + (datevalue.getMinutes() > 9 ? datevalue.getMinutes() : ("0" + datevalue.getMinutes())) + ":" + (datevalue.getSeconds() > 9 ? datevalue.getSeconds() : ("0" + datevalue.getSeconds()))
}
async function oauthVerification(url, method) {
  try {
    const auth = new GoogleAuth({
      scopes: process.env.API_AUTHSCOPE
    });
    const client = await auth.getClient();
    const response = await client.request({ url, method });
    return response;
  } catch (error) {
    console.log("ERROR : --------------------> ", error)
    throw error;
  }
}
function waitFor(milliseconds) {
  return new Promise((resolve) => setTimeout(resolve, milliseconds));
}
async function retryWithBackoff(promise, onRetry, maxRetries, retries = 0) {
  try {
    if (retries > 0) {
      const timeToWait = 2 ** retries * 100;
      console.log(`waiting for ${timeToWait}ms...`);
      await waitFor(timeToWait);
    }
    return await promise();
  } catch (e) {
    if (retries < maxRetries) {
      onRetry();
      return retryWithBackoff(promise, onRetry, maxRetries, retries + 1);
    } else {
      console.warn("Max retries reached. Bubbling the error up");
      throw e;
    }
  }
}
exports.ProcessDatasetDetails = async (event, context) => {
  try {
    let url = '';
    let datasetDetailsListData = []
    const bigquery = new BigQuery();
    const message = event.data ? Buffer.from(event.data, 'base64').toString() : 'DATASETS DATA NOT SUBSCRIBED';
    let datasetDetails = JSON.parse(message)
    console.log("datasetDetails:-------------->", datasetDetails);
    url = `${process.env.Google_BaseURL}/projects/${datasetDetails.projectId}/datasets/${datasetDetails.datasetId}`;
    const datasetsDetailResponse = await retryWithBackoff(
      () => oauthVerification(url),
      () => console.log("onRetry called..."),
      3
    );
    const datasetsDetail = datasetsDetailResponse.data
    let dCt = new Date(parseInt(datasetsDetail.creationTime))
    let datasetCreationTime = await dateTimeFormat(dCt)
    let dMt = new Date(parseInt(datasetsDetail.lastModifiedTime))
    let datasetModifiedTime = await dateTimeFormat(dMt)
    let datasetDetailsList = {
      RunId: datasetDetails.runId,
      ProjectId: datasetsDetail.datasetReference.projectId,
      DatasetId: datasetsDetail.datasetReference.datasetId,
      Location: datasetsDetail.location,
      CreatedDatetime: datasetCreationTime,
      LastModifiedDatetime: datasetModifiedTime,
      DatasetSchemaHash: null,
      LastAccessedTimestamp_DMLOnly: null
    }
    datasetDetailsListData.push(datasetDetailsList)
    await bigquery
      .dataset("MetricsExtraction")
      .table("Datasets")
      .insert(datasetDetailsListData);
    console.log(`Inserted ${datasetDetailsListData.length} rows`);
  } catch (err) {
    console.log("error:---------->-->", err);
  }
};