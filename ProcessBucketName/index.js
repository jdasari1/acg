const { Storage } = require('@google-cloud/storage');
var { PubSub } = require('@google-cloud/pubsub');
async function dateTimeFormat(datevalue) {
  return datevalue.getFullYear() + "-" + (datevalue.getMonth() + 1 > 9 ? datevalue.getMonth() + 1 : "0" + (datevalue.getMonth() + 1)) + "-" + ((datevalue.getDate() > 9 ? datevalue.getDate() : "0" + datevalue.getDate())) + " " + (datevalue.getHours() > 9 ? datevalue.getHours() : "0" + (datevalue.getHours())) + ":" + (datevalue.getMinutes() > 9 ? datevalue.getMinutes() : ("0" + datevalue.getMinutes())) + ":" + (datevalue.getSeconds() > 9 ? datevalue.getSeconds() : ("0" + datevalue.getSeconds()))
}
async function pubsub_publish(publisher, data) {
    try {
        const dataBuffer = Buffer.from(JSON.stringify(data));
        await publisher.publishMessage({ data: dataBuffer });
    } catch (error) {
        console.error(`Received error while publishing: ${error.message}`);
        process.exitCode = 1;
    }
}
exports.ProcessBucketName = async (event, context) => {
    try {
        const message = event.data ? Buffer.from(event.data, 'base64').toString() : 'BUCKET_NAME NOT SUBSCRIBED';
        let bucketnameData = JSON.parse(message);
        console.log("bucketnameData:-------------->", bucketnameData)
        const maxMessages = 10;
        const maxWaitTime = 10;
        var pubsubClient = new PubSub();
        const processStorageBucketSize_batchPublisher = pubsubClient.topic(process.env.STORAGE_BUCKET_SIZE_TOPICID, {
            batching: {
                maxMessages: maxMessages,
                maxMilliseconds: maxWaitTime * 100,
            },
        });
        const storage = new Storage();
        const [files] = await storage.bucket(bucketnameData.BucketName).getFiles();
        files.map(async file => {
            let updatedTime = new Date((file.metadata.updated))
            updatedTime = await dateTimeFormat(updatedTime);
            let bucketSizeDetails = {
                RunId: bucketnameData.RunId,
                ProjectId: bucketnameData.ProjectId,
                BucketId: bucketnameData.BucketName,
                FileName: file.name,
                FileSize: file.metadata.size,
                LastModifiedTimestamp: updatedTime
            }
            await pubsub_publish(processStorageBucketSize_batchPublisher, bucketSizeDetails);
            console.log("bucketSizeDetails:--------->", bucketSizeDetails);
        });
    } catch (err) {
        console.log("error:---------->", err)
    }
};
