const { BigQuery } = require('@google-cloud/bigquery');
const { v4: uuidv4 } = require('uuid');
const { PubSub } = require('@google-cloud/pubsub');
async function dateTimeFormat(datevalue) {
  return datevalue.getFullYear() + "-" + (datevalue.getMonth() + 1 > 9 ? datevalue.getMonth() + 1 : "0" + (datevalue.getMonth() + 1)) + "-" + ((datevalue.getDate() > 9 ? datevalue.getDate() : "0" + datevalue.getDate())) + " " + (datevalue.getHours() > 9 ? datevalue.getHours() : "0" + (datevalue.getHours())) + ":" + (datevalue.getMinutes() > 9 ? datevalue.getMinutes() : ("0" + datevalue.getMinutes())) + ":" + (datevalue.getSeconds() > 9 ? datevalue.getSeconds() : ("0" + datevalue.getSeconds()))
}
async function bigQueryFunction(projectId) {
  return new Promise((resolve, reject) => {
    try {
      var bigquery = new BigQuery({
        projectId: projectId
      })
      bigquery.getDatasets((err, Dataset) => {
        if (Dataset) {
          resolve(bigquery)
        } else {
          reject(err);
        }
      })
    } catch (err) {
      console.log("BIGQUERY CONNECTION ERROR : ######################### ", err)
      reject(err);
    }
  });
}
async function executeBigQuery(projectId, query) {
  try {
    const DBcon = await bigQueryFunction(projectId);
    const data = await DBcon.query(query);
    return data;
  } catch (error) {
    return error;
  }
}
async function pubsub_publish(publisher, data) {
  try {
    const dataBuffer = Buffer.from(JSON.stringify(data));
    await publisher.publishMessage({ data: dataBuffer });
  } catch (error) {
    console.error(`Received error while publishing: ${error.message}`);
    process.exitCode = 1;
  }
}
exports.AUDIT_RUN = async (req, res) => {
  let date = new Date()
  let auditCreationTime = await dateTimeFormat(date)
  let runId = await uuidv4();
  let user = "Cloud Scheduler";
  let query = "";
  const maxMessages = 10;
  const maxWaitTime = 10;
  const bigquery = new BigQuery();
  let auditTableDetails = []
  console.log("RUNID : ", runId)
  let auditTable = {
    runId: runId,
    startTimestamp: auditCreationTime,
    EndTimestamp: null,
    ExecutedBy: user,
    IsAuto: true
  }
  console.log("auditTable:----------->", auditTable);
  auditTableDetails.push(auditTable);
  try {
    await bigquery
      .dataset("MetricsExtraction")
      .table("AuditRuns")
      .insert(auditTableDetails);
    console.log(`Inserted ${auditTableDetails.length} rows`);
    query = `select ProjectId from datalake-dev-48126.Configuration.GCPProjects`;
    console.log("query:--------->", query)
    await executeBigQuery("datalake-dev-48126", query).then((result) => {
      projectResponse = result
      console.log("Getting Projects :-------------------------------->", result)
    })
    const pubSubClient = new PubSub();
    const project_batchPublisher = pubSubClient.topic(process.env.PROJECT_TOPICID, {
      batching: {
        maxMessages: maxMessages,
        maxMilliseconds: maxWaitTime * 1000,
      },
    });
    const excessPermissions_batchPublisher = pubSubClient.topic(process.env.EXCESS_PERMISSIONS_TOPICID, {
      batching: {
        maxMessages: maxMessages,
        maxMilliseconds: maxWaitTime * 1000,
      },
    });
    const buckets_batchPublisher = pubSubClient.topic(process.env.BUCKETS_TOPICID, {
      batching: {
        maxMessages: maxMessages,
        maxMilliseconds: maxWaitTime * 1000,
      },
    });
    if (projectResponse && projectResponse[0] && projectResponse.length) {
      projectResponse[0].map(async function (project) {
        project.runId = runId
        project.projectId = project.ProjectId
        await pubsub_publish(project_batchPublisher, project);
        await pubsub_publish(buckets_batchPublisher, project);
        await pubsub_publish(excessPermissions_batchPublisher, project)
        console.log("project:----->", project);
      })
    }
    else {
      console.log("No projects Available in Bigquery")
    }
    //     //##############################UPDATING AUDIT END TIME####################################################### 
    res.json({ "AUDIT_RUN:------->": "TESTING TEST CLOUD FUNCTION " })
  } catch (error) {
    console.log("error :--------->-->", error)
    res.json({ "ERROR": error })
  }
};