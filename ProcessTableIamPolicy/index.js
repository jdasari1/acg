const { BigQuery } = require('@google-cloud/bigquery');
const { GoogleAuth } = require('google-auth-library');
async function oauthVerification(url, method) {
  try {
    const auth = new GoogleAuth({
      scopes: process.env.API_AUTHSCOPE
    });
    const client = await auth.getClient();
    const response = await client.request({ url, method });
    return response;
  } catch (error) {
    console.log("ERROR : --------------------> ", error)
    throw error;
  }
}
function waitFor(milliseconds) {
  return new Promise((resolve) => setTimeout(resolve, milliseconds));
}
async function retryWithBackoff(promise, onRetry, maxRetries, retries = 0) {
  try {
    if (retries > 0) {
      const timeToWait = 2 ** retries * 100;
      console.log(`waiting for ${timeToWait}ms...`);
      await waitFor(timeToWait);
    }
    return await promise();
  } catch (e) {
    if (retries < maxRetries) {
      onRetry();
      return retryWithBackoff(promise, onRetry, maxRetries, retries + 1);
    } else {
      console.warn("Max retries reached. Bubbling the error up");
      throw e;
    }
  }
}
exports.ProcessTableIamPolicy = async (event, context) => {
  try {
    let tableIamPolicyListData = []
    const bigquery = new BigQuery();
    const message = Buffer.from(event.data, 'base64').toString();
    console.log(" table Id's ", message);
    let tableData = JSON.parse(message);
    //##############################GETTING USER IAM POLICY FOR TABLES########################################################
    url = `${process.env.Google_BaseURL}/projects/${tableData.projectId}/datasets/${tableData.datasetId}/tables/${tableData.tableId}:getIamPolicy`;
    const userPermissions = await retryWithBackoff(
      () => oauthVerification(url),
      () => console.log("onRetry called..."),
      3
    );
    if (userPermissions && userPermissions.data && userPermissions.data.bindings && userPermissions.data.bindings.length) {
      for (let accessData of userPermissions.data.bindings) {
        console.log("GETTING accessData FROM THE USERPERMISSIONS----->", accessData);
        let role = accessData.role.split("roles/").pop();
        let member = accessData.members[0].split(":")
        let acccountUser = member[1];
        let accountType = member[0];
        let tableIamPolicyList = {
          RunId: tableData.runId,
          ProjectId: tableData.projectId,
          DatasetId: tableData.datasetId,
          TableId: tableData.tableId,
          AccountType: accountType,
          Role: role,
          AccountUser: acccountUser
        }
        tableIamPolicyListData.push(tableIamPolicyList)
        await bigquery
          .dataset("MetricsExtraction")
          .table("UserAccessList")
          .insert(tableIamPolicyListData);
        console.log(`Inserted ${tableIamPolicyListData.length} rows`);
      }
    }
    else {
      console.log("TABLE USERACCESSLIST DOESN'T EXIST");
    }
  } catch (err) {
    console.log("error:-------------->--->", err);
  }
};