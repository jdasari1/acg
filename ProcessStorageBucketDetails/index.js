const { BigQuery } = require('@google-cloud/bigquery');
exports.ProcessStorageBucketDetails = async (event, context) => {
    try {
        let storageBucketDataList = []
        const bigquery = new BigQuery();
        const message = event.data ? Buffer.from(event.data, 'base64').toString() : 'STORAGE BUCKET ID NOT SUBSCRIBED';
        let storageBucketData = JSON.parse(message);
        storageBucketDataList.push(storageBucketData);
        console.log("storageBucketDataList:------------>", storageBucketDataList)
        // #################################INSERTING DATA INTO THE BUCKETS TABLE##########################################//
        // Insert data into a table
        await bigquery
            .dataset("MetricsExtraction")
            .table("StorageBuckets")
            .insert(storageBucketDataList);
        console.log(`Inserted ${storageBucketDataList.length} rows`);
    } catch (err) {
        console.log("error:--------->-->", err)
    }
};