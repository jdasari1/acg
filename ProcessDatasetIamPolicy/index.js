const { BigQuery } = require('@google-cloud/bigquery');
const { GoogleAuth } = require('google-auth-library');
async function oauthVerification(url, method) {
  try {
    const auth = new GoogleAuth({
      scopes: process.env.API_AUTHSCOPE
    });
    const client = await auth.getClient();
    const response = await client.request({ url, method });
    return response;
  } catch (error) {
    console.log("ERROR : --------------------> ", error)
    throw error;
  }
}
function waitFor(milliseconds) {
  return new Promise((resolve) => setTimeout(resolve, milliseconds));
}
async function retryWithBackoff(promise, onRetry, maxRetries, retries = 0) {
  try {
    if (retries > 0) {
      const timeToWait = 2 ** retries * 100;
      console.log(`waiting for ${timeToWait}ms...`);
      await waitFor(timeToWait);
    }
    return await promise();
  } catch (e) {
    if (retries < maxRetries) {
      onRetry();
      return retryWithBackoff(promise, onRetry, maxRetries, retries + 1);
    } else {
      console.warn("Max retries reached. Bubbling the error up");
      throw e;
    }
  }
}
exports.ProcessDatasetIamPolicy = async (event, context) => {
  try {
    let url = '';
    let query = '';
    let datasetIamPolicyListData = []
    const bigquery = new BigQuery();
    const message = event.data
      ? Buffer.from(event.data, 'base64').toString() : 'NO DATASETS SUBSCRIBED';
    console.log("RESPONSE : ", message);
    let datasetData = JSON.parse(message)
    url = `${process.env.Google_BaseURL}/projects/${datasetData.projectId}/datasets/${datasetData.datasetId}`;
    const datasetsDetailResponse = await retryWithBackoff(
      () => oauthVerification(url),
      () => console.log("onRetry called..."),
      3
    );
    let datasetsDetail = datasetsDetailResponse.data
    let datasets = datasetsDetail.datasetReference
    // //##############################INSERTING DATASET ACCESS DETAILS TO USER ACCESS LIST########################################################
    if (datasetsDetail && datasetsDetail.access && datasetsDetail.access.length) {
      let accountType = '';
      let accountUser = '';
      for (let accessData of datasetsDetail.access) {
        if (accessData && accessData.role) {
          let role = accessData.role.split("roles/").pop();
          if (accessData && accessData.userByEmail) {
            accountUser = accessData.userByEmail
            if ((accessData.userByEmail).includes("iam.gserviceaccount.com")) {
              accountType = "serviceAccount";
            }
            else {
              accountType = "userAccount"
            }
            let datasetIamPolicyList = {
              RunId: datasetData.runId,
              ProjectId: datasets.projectId,
              DatasetId: datasets.datasetId,
              TableId: null,
              AccountType: accountType,
              Role: role,
              AccountUser: accountUser
            }
            datasetIamPolicyListData.push(datasetIamPolicyList)
            await bigquery
              .dataset("MetricsExtraction")
              .table("UserAccessList")
              .insert(datasetIamPolicyListData);
            console.log(`Inserted ${datasetIamPolicyListData.length} rows`);
          }
          else {
            console.log("DATASET  USERACCESSLIST ROLES DOESN'T EXIST");
          }
        }
        else {
          console.log("DATASET  USERACCESSLIST DOENT EXIST");
        }
      }
    }
  } catch (err) {
    console.log("error:--------->--->", err)
  }
};
