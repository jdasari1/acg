const { v4: uuidv4 } = require('uuid');
const { BigQuery } = require('@google-cloud/bigquery');
const { PubSub } = require('@google-cloud/pubsub');
const dateTimeFormat = async (datevalue) => {
    return datevalue.getFullYear() + "-" + (datevalue.getMonth() + 1 > 9 ? datevalue.getMonth() + 1 : "0" + (datevalue.getMonth() + 1)) + "-" + ((datevalue.getDate() > 9 ? datevalue.getDate() : "0" + datevalue.getDate())) + " " + (datevalue.getHours() > 9 ? datevalue.getHours() : "0" + (datevalue.getHours())) + ":" + (datevalue.getMinutes() > 9 ? datevalue.getMinutes() : ("0" + datevalue.getMinutes())) + ":" + (datevalue.getSeconds() > 9 ? datevalue.getSeconds() : ("0" + datevalue.getSeconds()))
}
async function bigQueryFunction(projectId) {
    return new Promise((resolve, reject) => {
        try {
            var bigquery = new BigQuery({
                projectId: projectId
            })
            bigquery.getDatasets((err, Dataset) => {
                if (Dataset) {
                    resolve(bigquery)
                } else {
                    reject(err);
                }
            })
        } catch (err) {
            console.log("BIGQUERY CONNECTION ERROR : ######################### ", err)
            reject(err);
        }
    });
}
async function executeBigQuery(projectId, query) {
    try {
        const DBcon = await bigQueryFunction(projectId);
        const data = await DBcon.query(query);
        return data;
    } catch (error) {
        return error;
    }
}
async function pubsub_publish(publisher, data) {
    try {
        const dataBuffer = Buffer.from(JSON.stringify(data));
        await publisher.publishMessage({ data: dataBuffer });
    } catch (error) {
        console.error(`Received error while publishing: ${error.message}`);
        process.exitCode = 1;
    }
}
exports.DeletionEvents_Invoker = async (req, res) => {
    let query = "";
    let runId = await uuidv4();
    let date = new Date()
    let startTimestamp = await dateTimeFormat(date)
    let TDcount = 0;
    let QLcount = 0;
    console.log("runId : ", runId);
    console.log("startTimestamp: ", startTimestamp)
    query = `select ProjectId from datalake-dev-48126.Configuration.GCPProjects`;
    console.log("query:-------->", query)
    await executeBigQuery("datalake-dev-48126", query).then((result) => {
        projects = result;
        console.log("Getting Projects :-------------------------------->", projects)
    })
    let projectResponse = []
    projects[0].map(result => {
        projectResponse.push(result.ProjectId);
    })
    let wholeProjects = projectResponse.toString();
    var result = "('" + wholeProjects.split(",").join("','") + "')";
    const maxMessages = 10;
    const maxWaitTime = 10;
    let TDskip = 0
    let QLskip = 0
    let TDlimit = 500
    let QLlimit = 500
    try {
        const pubSubClient = new PubSub();
        const deletionEvent_batchPublisher = pubSubClient.topic(process.env.DELETIONEVENT_TOPICID, {
            batching: {
                maxMessages: maxMessages,
                maxMilliseconds: maxWaitTime * 1000,
            }
        });
        const querylogs_batchPublisher = pubSubClient.topic(process.env.QUERYLOGS_TOPICID, {
            batching: {
                maxMessages: maxMessages,
                maxMilliseconds: maxWaitTime * 1000,
            },
        });
        if (projects[0] && projects.length) {
            query = `SELECT LastProcessedTDTimestamp,LastProcessedQLTimestamp FROM datalake-dev-48126.Configuration.LastProcessedTimestamp`;
            var lastProcessedTime = await executeBigQuery("datalake-dev-48126", query).then((result) => { return result })
            var lastProcessed = lastProcessedTime.flat(1)
            let ltt = new Date(lastProcessed[0].LastProcessedTDTimestamp.value)
            let lastTDTime = await dateTimeFormat(ltt);
            let lqt = new Date(lastProcessed[0].LastProcessedQLTimestamp.value)
            let lastQLTime = await dateTimeFormat(lqt);
            query = 'SELECT count(ProjectId) as tdCount, max(DeleteTimestamp) as TDDeleteTimestamp FROM `datalake-dev-48126.MetricsExtraction.DeletionEventView` where DeleteTimestamp>' + `'${lastTDTime}'` + `and ProjectId in ${result}`;
            TDcount = await executeBigQuery("datalake-dev-48126", query);
            console.log("TDcount :------------> ", TDcount[0][0]["tdCount"]);
            let latestTDTime = TDcount[0][0]['TDDeleteTimestamp'] !== null ? `${await dateTimeFormat(new Date(TDcount[0][0]['TDDeleteTimestamp'].value))}` : lastTDTime
            let TDrecords = TDcount[0][0]["tdCount"];
            for (let i = 0; i < TDrecords; i++) {
                if (TDrecords - TDskip < TDlimit) {
                    TDlimit = TDrecords - TDskip
                }
                let data = {
                    skip: TDskip,
                    limit: TDlimit,
                    lastTDTime: lastTDTime,
                    runId: runId,
                    projectId: result,
                    startTimestamp: startTimestamp,
                    endTimestamp: latestTDTime
                }
                await pubsub_publish(deletionEvent_batchPublisher, data)
                TDskip = TDskip + TDlimit;
                i = TDskip;
            }

            query = "SELECT count(projectId) as qlCount, max(startTime) as QLstartTime FROM `datalake-dev-48126.MetricsExtraction.QueryLogsView` where startTime>" + `'${lastQLTime}'` + ` and projectId in ` + `${result}`;
            QLcount = await executeBigQuery("datalake-dev-48126", query)
            console.log("QLcount :------> ", QLcount[0][0]['qlCount']);
            let latestQLTime = QLcount[0][0]['QLstartTime'] !== null ? `${await dateTimeFormat(new Date(QLcount[0][0]['QLstartTime'].value))}` : lastQLTime
            let QLrecords = QLcount[0][0]['qlCount']
            for (let i = 0; i < QLrecords; i++) {
                if (QLrecords - QLskip < QLlimit) {
                    QLlimit = QLrecords - QLskip
                }
                let data = {
                    skip: QLskip,
                    limit: QLlimit,
                    lastQLTime: lastQLTime,
                    runId: runId,
                    projectId: result,
                    startTimestamp: startTimestamp,
                    endTimestamp: latestQLTime
                }
                await pubsub_publish(querylogs_batchPublisher, data)
                QLskip = QLskip + QLlimit
                i = QLskip;
            }
            query = "UPDATE `datalake-dev-48126.Configuration`.LastProcessedTimestamp SET  LastProcessedTDTimestamp=" + `'${latestTDTime}', LastProcessedQLTimestamp=` + `'${latestQLTime}' where true`;
            console.log("query:-------------------->", query);
            await executeBigQuery("datalake-dev-48126", query).then((result) => { console.log("Updating last processd time in BigQuery :-----------> ", result) })
        }
        else {
            console.log("No projects Available in Bigquery")
        }
        res.json({ "NOTIFICATION INVOKER": "COMPLETED" })
    } catch (error) {
        console.log("ERROR IN DELETION EVENTS INVOKER  :", error)
        res.json({ "ERROR---->--->-->": error })
    }
};

