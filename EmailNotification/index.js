const { BigQuery } = require('@google-cloud/bigquery');
var nodemailer = require('nodemailer');
const json2html = require('node-json2html');
async function bigQueryFunction(projectId) {
  return new Promise((resolve, reject) => {
    try {
      var bigquery = new BigQuery({
        projectId: projectId
      })
      bigquery.getDatasets((err, Dataset) => {
        if (Dataset) {
          resolve(bigquery)
        } else {
          reject(err);
        }
      })
    } catch (err) {
      console.log("BIGQUERY CONNECTION ERROR : ######################### ", err)
      reject(err);
    }
  });
}
async function executeBigQuery(projectId, query) {
  try {
    const DBcon = await bigQueryFunction(projectId);
    const data = await DBcon.query(query);
    return data;
  } catch (error) {
    return error;
  }
}
let template_table_header = {
  "<>": "tr", "html": [
    { "<>": "th", "html": "Project" },
    { "<>": "th", "html": "Dataset" },
    { "<>": "th", "html": "Table" },
    { "<>": "th", "html": "Deleted On" }
  ]
}
let template_table_body = {
  "<>": "tr", "html": [
    { "<>": "td", "html": "${ProjectId}" },
    { "<>": "td", "html": "${DatasetId}" },
    { "<>": "td", "html": "${TableId}" },
    { "<>": "td", "html": "${DeletionTimeStamp}" }
  ]
}
exports.EmailNotification = async (req, res) => {
  try {
    query = `SELECT distinct EmailAddress FROM datalake-dev-48126.Configuration.Notification`;
    let emailAddress = await executeBigQuery("datalake-dev-48126", query);
    console.log("emailAddress:---------->", emailAddress);
    query = `SELECT ProjectId, DatasetId, TableId, DeleteTimestamp, ExecutedBy, Reason
FROM datalake-dev-48126.Dashboard.DataRecovery WHERE DeleteTimestamp < (SELECT LastProcessedTDTimestamp FROM datalake-dev-48126.Configuration.LastProcessedTimestamp)`;
    let deletionData = await executeBigQuery("datalake-dev-48126", query);
    let modifiedData = []
    var delData = deletionData.flat(1)
    delData.map(result => {
      var obj = {
        ProjectId: result.ProjectId,
        DatasetId: result.DatasetId,
        TableId: result.TableId,
        DeletionTimeStamp: result.DeleteTimestamp.value.replace("T", " ")
      }
      modifiedData.push(obj);
    })
    let data = modifiedData;
    let table_header = json2html.transform(data[0], template_table_header);
    let table_body = json2html.transform(data, template_table_body);

    let header = '<!DOCTYPE html>' + '<html lang="en">\n'
    let body = '<table border="1" id="my_table">\n<thead>' + table_header + '\n</thead>\n<tbody>\n' + table_body + '\n</tbody>\n</table>'
    body = '<body>' + body + '</body>'
    let html = header + body + '</html>';
    //###################SEND EMAIL NOTIFICATION################################   
    let emails = emailAddress.flat(1);
    emails.map(email => {
      let transporter = nodemailer.createTransport({
        host: process.env.HOST,
        port: process.env.PORTNUMBER,
        secure: false, // true for 465, false for other ports
        auth: {
          user: process.env.USER, // generated ethereal user
          pass: process.env.PASSWORD, // generated ethereal password
        },
      });
      var mailOptions = {
        from: process.env.FROM,
        to: `${email.EmailAddress}`,
        subject: 'Table Deletion Events',
        html: html
      };
      transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
          console.log(error);
        } else {
          console.log('Email sent: ' + info.response);
        }
      });
    })
    res.json({ "EmailNotification": `EmailNotification` })
  } catch (err) {
    console.log("error:------------>--->", err)
  }
};
