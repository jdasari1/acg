const { GoogleAuth } = require('google-auth-library');
const { PubSub } = require('@google-cloud/pubsub');
const oauthVerification = async (url, method) => {
  try {
    const auth = new GoogleAuth({
      scopes: process.env.API_AUTHSCOPE
    });
    var client = await auth.getClient();
    const response = await client.request({ url, method });
    return response;
  } catch (error) {
    return error;
  }
}
function waitFor(milliseconds) {
  return new Promise((resolve) => setTimeout(resolve, milliseconds));
}
async function retryWithBackoff(promise, onRetry, maxRetries, retries = 0) {
  try {
    if (retries > 0) {
      const timeToWait = 2 ** retries * 100;
      console.log(`waiting for ${timeToWait}ms...`);
      await waitFor(timeToWait);
    }
    return await promise();
  } catch (e) {
    if (retries < maxRetries) {
      onRetry();
      return retryWithBackoff(promise, onRetry, maxRetries, retries + 1);
    } else {
      console.warn("Max retries reached. Bubbling the error up");
      throw e;
    }
  }
}
async function pubsub_publish(publisher, data) {
  try {
    const dataBuffer = Buffer.from(JSON.stringify(data));
    await publisher.publishMessage({ data: dataBuffer });
  } catch (error) {
    console.error(`Received error while publishing: ${error.message}`);
    process.exitCode = 1;
  }
}
exports.ProcessExcessPermissions = async (event, context) => {
  try {
    const message = event.data ? Buffer.from(event.data, 'base64').toString() : 'DATA NOT RECIEVEED';
    let ProjectData = JSON.parse(message);
    console.log("ProjectData----------->", ProjectData);
    const maxMessages = 10;
    const maxWaitTime = 10;
    let excessPermissionsList = []
    var pubsubClient = new PubSub();
    const processExcessPermissionsDetails_batchPublisher = pubsubClient.topic(process.env.EXCESS_PERMISSIONS_DETAILS_TOPICID, {
      batching: {
        maxMessages: maxMessages,
        maxMilliseconds: maxWaitTime * 50,
      },
    });
    let url = '';
    // ////////////////////////////////////////////////  #FETCHING LIST OF DATASETS#  ////////////////////////////////////////////////////////////////////////
    url = `https://recommender.googleapis.com/v1/projects/${ProjectData.projectId}/locations/global/insightTypes/google.iam.policy.Insight/insights`;
    console.log("url---------->", url)
    const InsightsList = await retryWithBackoff(
      () => oauthVerification(url),
      () => console.log("onRetry called..."),
      3
    );
    if (InsightsList && InsightsList.data && InsightsList.data.insights) {
      let insightListResponse = InsightsList.data.insights;
      for (const insightResp of insightListResponse) {
        console.log("Member", insightResp.content.member)
        let all_exercisedPermissions = insightResp.content.exercisedPermissions
        let member = insightResp.content.member
        member = member.replace(/[}{"']/g, '');
        member = member.split(":")
        let permissionsUsed = insightResp.description
        permissionsUsed = permissionsUsed.split(' ')
        console.log("member : ", member, "  permissions : ", permissionsUsed)
        if (insightResp.content.exercisedPermissions && insightResp.content.exercisedPermissions.length) {
          for (const data of all_exercisedPermissions) {
            let excessPermissionsData = {
              RunId: ProjectData.runId,
              ProjectId: ProjectData.projectId,
              Role: insightResp.content.role,
              AccountType: member[0],
              AccountUser: member[1],
              PermissionsUsed: data.permission,
              TotalPermissions: insightResp.content.currentTotalPermissionsCount,
              UsedPermissions: permissionsUsed[0]
            }
            excessPermissionsList.push(excessPermissionsData)
          }
        }
        else {
          let excessPermissionsData = {
            RunId: ProjectData.runId,
            ProjectId: ProjectData.projectId,
            Role: insightResp.content.role,
            AccountType: member[0],
            AccountUser: member[1],
            PermissionsUsed: '',
            TotalPermissions: insightResp.content.currentTotalPermissionsCount,
            UsedPermissions: permissionsUsed[0]
          }
          excessPermissionsList.push(excessPermissionsData)
        }
      }
    }
    for (let i = 0; i < excessPermissionsList.length; i += 20) { // Loop through the array with a step of 20
      let subArr = excessPermissionsList.slice(i, i + 20); // Get 20 elements starting from the current index
      console.log("subArr:-------------->", subArr);
      await pubsub_publish(processExcessPermissionsDetails_batchPublisher, subArr);
    }
  } catch (err) {
    console.log("error:---->-->", err)
  }
};