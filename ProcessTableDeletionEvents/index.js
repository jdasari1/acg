const { BigQuery } = require('@google-cloud/bigquery');
const { PubSub } = require('@google-cloud/pubsub');
async function dateTimeFormat(datevalue) {
  return datevalue.getFullYear() + "-" + (datevalue.getMonth() + 1 > 9 ? datevalue.getMonth() + 1 : "0" + (datevalue.getMonth() + 1)) + "-" + ((datevalue.getDate() > 9 ? datevalue.getDate() : "0" + datevalue.getDate())) + " " + (datevalue.getHours() > 9 ? datevalue.getHours() : "0" + (datevalue.getHours())) + ":" + (datevalue.getMinutes() > 9 ? datevalue.getMinutes() : ("0" + datevalue.getMinutes())) + ":" + (datevalue.getSeconds() > 9 ? datevalue.getSeconds() : ("0" + datevalue.getSeconds()))
}
async function pubsub_publish(publisher, data) {
    try {
        const dataBuffer = Buffer.from(JSON.stringify(data));
        await publisher.publishMessage({ data: dataBuffer });
    } catch (error) {
        console.error(`Received error while publishing: ${error.message}`);
        process.exitCode = 1;
    }
}
async function bigQueryFunction(projectId) {
  return new Promise((resolve, reject) => {
    try {
      var bigquery = new BigQuery({
        projectId: projectId
      })
      bigquery.getDatasets((err, Dataset) => {
        if (Dataset) {
          resolve(bigquery)
        } else {
          reject(err);
        }
      })
    } catch (err) {
      console.log("BIGQUERY CONNECTION ERROR : ######################### ", err)
      reject(err);
    }
  });
}
async function executeBigQuery(projectId, query) {
  try {
    const DBcon = await bigQueryFunction(projectId);
    const data = await DBcon.query(query);
    return data;
  } catch (error) {
    return error;
  }
}
exports.ProcessTableDeletionEvents = async (event, context) => {
  try {
    let query = "";
    const message = event.data ? Buffer.from(event.data, 'base64').toString() : 'USER DATA NOT SUBSCRIBED';
    let projectResponse = JSON.parse(message)
    const maxMessages = 10;
        const maxWaitTime = 10;
        const pubsubClient = new PubSub();
        const processTableDeletionInsert_batchPublisher = pubsubClient.topic(process.env.TABLE_DELETION_INSERT_TOPICID, {
            batching: {
                maxMessages: maxMessages,
                maxMilliseconds: maxWaitTime * 100,
            },
        });
    console.log("projectResponse : ", projectResponse)
    //##############################GETTING LIST OF ALL DeletionEvents#######################################################
    query = "SELECT ProjectId, DatasetId, TableId, DeleteTimestamp, UserAgent, CallerIp, ExecutedBy, Reason FROM `datalake-dev-48126.MetricsExtraction.DeletionEventView` where projectId IN " + `${projectResponse.projectId}` + ` and DeleteTimestamp> ` + `'${projectResponse.lastTDTime}' and DeleteTimestamp < ` + `'${projectResponse.endTimestamp}'` + `limit ${projectResponse.limit} OFFSET ${parseInt(projectResponse.skip)}`;
    console.log("QUERY  :--------------------> ", query)
    let deletedTables = await executeBigQuery("datalake-dev-48126", query)
    if (deletedTables && deletedTables[0] && deletedTables[0].length) {
      deletedTables[0].map(async function (data) {
        let receiveTimestamp = new Date(data.DeleteTimestamp.value)
        receiveTimestamp = await dateTimeFormat(receiveTimestamp)
        //  ##############################INSERTING LIST OF ALL DeletionEvents#######################################################
        let tableDeleteList = {
          RunId: projectResponse.runId,
          ProjectId: data.ProjectId,
          DatasetId: data.DatasetId,
          TableId: data.TableId,
          DeleteTimestamp: receiveTimestamp,
          UserAgent: data.UserAgent,
          CallerIp: data.CallerIp,
          ExecutedBy: data.ExecutedBy,
          Reason: data.Reason,
          StartTimestamp: projectResponse.startTimestamp
        }
        await pubsub_publish(processTableDeletionInsert_batchPublisher, tableDeleteList);
      })
    }
  } catch (err) {
    console.log("error:----------->--->-->", err)
  }
};
