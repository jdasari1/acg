const { GoogleAuth } = require('google-auth-library');
const { PubSub } = require('@google-cloud/pubsub');
async function dateTimeFormat(datevalue) {
  return datevalue.getFullYear() + "-" + (datevalue.getMonth() + 1 > 9 ? datevalue.getMonth() + 1 : "0" + (datevalue.getMonth() + 1)) + "-" + ((datevalue.getDate() > 9 ? datevalue.getDate() : "0" + datevalue.getDate())) + " " + (datevalue.getHours() > 9 ? datevalue.getHours() : "0" + (datevalue.getHours())) + ":" + (datevalue.getMinutes() > 9 ? datevalue.getMinutes() : ("0" + datevalue.getMinutes())) + ":" + (datevalue.getSeconds() > 9 ? datevalue.getSeconds() : ("0" + datevalue.getSeconds()))
}
async function oauthVerification(url, method) {
  try {
    const auth = new GoogleAuth({
      scopes: process.env.API_AUTHSCOPE
    });
    const client = await auth.getClient();
    const response = await client.request({ url, method });
    return response;
  } catch (error) {
    console.log("ERROR : -------------------> ", error)
    throw error;
  }
}
function waitFor(milliseconds) {
  return new Promise((resolve) => setTimeout(resolve, milliseconds));
}
async function retryWithBackoff(promise, onRetry, maxRetries, retries = 0) {
  try {
    if (retries > 0) {
      const timeToWait = 2 ** retries * 100;
      console.log(`waiting for ${timeToWait}ms...`);
      await waitFor(timeToWait);
    }
    return await promise();
  } catch (e) {
    if (retries < maxRetries) {
      onRetry();
      return retryWithBackoff(promise, onRetry, maxRetries, retries + 1);
    } else {
      console.warn("Max retries reached. Bubbling the error up");
      throw e;
    }
  }
}
async function pubsub_publish(publisher, data) {
  try {
    const dataBuffer = Buffer.from(JSON.stringify(data));
    await publisher.publishMessage({ data: dataBuffer });
  } catch (error) {
    console.error(`Received error while publishing: ${error.message}`);
    process.exitCode = 1;
  }
}
exports.ProcessRoutineIds = async (event, context) => {
  try {
    const pubSubClient = new PubSub();
    let url = '';
    const message = event.data
      ? Buffer.from(event.data, 'base64').toString()
      : 'Hello, World';
    console.log("message:------------>", message);
    let routinesResponse = JSON.parse(message);
    //##############################GETTING DATASET ROUTINES########################################################
    url = `${process.env.Google_BaseURL}/projects/${routinesResponse.projectId}/datasets/${routinesResponse.datasetId}/routines?maxResults=1000`;
    console.log("URL:------->", url);
    const routinesList = await retryWithBackoff(
      () => oauthVerification(url),
      () => console.log("onRetry called..."),
      3
    );
    console.log("ROUTINES LIST URL :   ROUTINES LIST RESPONSE ", routinesList.data);
    //  ##############################INSERTING ROUTINES INTO DATASET ROUTINES TABLES########################################################
    if (routinesList && routinesList.data && routinesList.data.routines) {
      let routineCreationTime = new Date(parseInt(routinesList.data.routines[0].creationTime))
      routineCreationTime = await dateTimeFormat(routineCreationTime)
      let routineLastModifiedTime = new Date(parseInt(routinesList.data.routines[0].lastModifiedTime))
      routineLastModifiedTime = await dateTimeFormat(routineLastModifiedTime)
      let datasetRoutineList = {
        RunId: routinesResponse.runId,
        ProjectId: routinesResponse.projectId,
        DatasetId: routinesResponse.datasetId,
        RoutineId: routinesList.data.routines[0].routineReference.routineId,
        RoutineType: routinesList.data.routines[0].routineType,
        CreationTimestamp: routineCreationTime,
        LastModifiedTimestamp: routineLastModifiedTime
      }
      const routinesDetails_batchPublisher = pubSubClient.topic(process.env.ROUTINES_DETAILS_TOPICID, {
        batching: {
          maxMessages: maxMessages,
          maxMilliseconds: maxWaitTime * 1000,
        },
      });
      await pubsub_publish(routinesDetails_batchPublisher, datasetRoutineList);
    }
    else {
      console.log("No routines are found");
    }
  } catch (err) {
    console.log("error:-------------->-->", err)
  }
};
