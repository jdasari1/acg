const { BigQuery } = require('@google-cloud/bigquery');
const { PubSub } = require(`@google-cloud/pubsub`);
var crypto = require('crypto');
async function dateTimeFormat(datevalue) {
  return datevalue.getFullYear() + "-" + (datevalue.getMonth() + 1 > 9 ? datevalue.getMonth() + 1 : "0" + (datevalue.getMonth() + 1)) + "-" + ((datevalue.getDate() > 9 ? datevalue.getDate() : "0" + datevalue.getDate())) + " " + (datevalue.getHours() > 9 ? datevalue.getHours() : "0" + (datevalue.getHours())) + ":" + (datevalue.getMinutes() > 9 ? datevalue.getMinutes() : ("0" + datevalue.getMinutes())) + ":" + (datevalue.getSeconds() > 9 ? datevalue.getSeconds() : ("0" + datevalue.getSeconds()))
}
async function bigQueryFunction(projectId) {
  return new Promise((resolve, reject) => {
    try {
      var bigquery = new BigQuery({
        projectId: projectId
      })
      bigquery.getDatasets((err, Dataset) => {
        if (Dataset) {
          resolve(bigquery)
        } else {
          reject(err);
        }
      })
    } catch (err) {
      console.log("BIGQUERY CONNECTION ERROR : ######################### ", err)
      reject(err);
    }
  });
}
async function executeBigQuery(projectId, query) {
  try {
    const DBcon = await bigQueryFunction(projectId);
    const data = await DBcon.query(query);
    return data;
  } catch (error) {
    return error;
  }
}
async function pubsub_publish(publisher, data) {
    try {
        const dataBuffer = Buffer.from(JSON.stringify(data));
        await publisher.publishMessage({ data: dataBuffer });
    } catch (error) {
        console.error(`Received error while publishing: ${error.message}`);
        process.exitCode = 1;
    }
}
exports.ProcessQueryLogs = async (event, context) => {
  try {
    const message = event.data ? Buffer.from(event.data, 'base64').toString() : 'NO DATA SUNSCRIBED';
    let query = '';
    let projectData = JSON.parse(message)
    const maxMessages = 10;
        const maxWaitTime = 10;
        const pubsubClient = new PubSub();
        const processQueryLogInsert_batchPublisher = pubsubClient.topic(process.env.QUERY_LOG_INSERT_TOPICID, {
            batching: {
                maxMessages: maxMessages,
                maxMilliseconds: maxWaitTime * 10,
            },
        });
    console.log("Getting Project data:------------->", projectData)
    //##############################GETTING ALL  SQL QUERY LOGS AND EUN WITH COST SPEND#######################################################
    query = "SELECT projectId,datasetId, query, executionTime, memoryUsage, costSpend, executedBy, source_of_execution, startTime, endTime FROM `datalake-dev-48126.MetricsExtraction.QueryLogsView` where projectid IN " + `${projectData.projectId}` + " and startTime>" + `'${projectData.lastQLTime}' and startTime < ` + `'${projectData.endTimestamp}' limit ${projectData.limit} OFFSET ${parseInt(projectData.skip)}`;
    let SQLQueryLogs = await executeBigQuery("datalake-dev-48126", query)
    console.log("SQLQueryLogs:----------------->", SQLQueryLogs);
    if (SQLQueryLogs && SQLQueryLogs[0] && SQLQueryLogs[0].length) {
      SQLQueryLogs[0].map(async function (querydata) {
        let queryHash = crypto.createHash('md5').update(querydata.query).digest('hex');
        let queryStartTimestamp = new Date(querydata.startTime.value);
        queryStartTimestamp = await dateTimeFormat(queryStartTimestamp);
        let queryEndTimestamp = new Date(querydata.endTime.value);
        queryEndTimestamp = await dateTimeFormat(queryEndTimestamp);
        //##############################INSERTING IN QUERYLOGS TABLE IN MYSQL#######################################################
        let queryLogsList = {
          RunId: projectData.runId,
          ProjectId: querydata.projectId,
          DatasetId: querydata.datasetId,
          SQLQuery: querydata.query,
          ExecutionTime_InMilliseconds: querydata.executionTime,
          MemoryUsed_InBytes: querydata.memoryUsage,
          CostSpend_InUSD: querydata.costSpend,
          ExecutedBy: querydata.executedBy,
          SQLQueryHash: queryHash,
          SQLQueryStartTimestamp: queryStartTimestamp,
          SQLQueryEndTimestamp: queryEndTimestamp,
          SourceOfExecution: querydata.source_of_execution,
          StartTimestamp: projectData.startTimestamp
        }
       await pubsub_publish(processQueryLogInsert_batchPublisher, queryLogsList);
      })
    }
    console.log("EXECUTED SUCESSFULLY:")
  } catch (error) {
    console.log("ERROR :----->--->--->", error)
  }
};
