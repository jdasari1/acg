var { GoogleAuth } = require('google-auth-library');
var { PubSub } = require('@google-cloud/pubsub');
async function oauthVerification(url, method) {
  try {
    const auth = new GoogleAuth({
      scopes: process.env.API_AUTHSCOPE
    });
    const client = await auth.getClient();
    const response = await client.request({ url, method });
    return response;
  } catch (error) {
    console.log("ERROR : --------------------> ", error)
    throw error;
  }
}
function waitFor(milliseconds) {
  return new Promise((resolve) => setTimeout(resolve, milliseconds));
}
async function retryWithBackoff(promise, onRetry, maxRetries, retries = 0) {
  try {
    if (retries > 0) {
      const timeToWait = 2 ** retries * 100;
      console.log(`waiting for ${timeToWait}ms...`);
      await waitFor(timeToWait);
    }
    return await promise();
  } catch (e) {
    if (retries < maxRetries) {
      onRetry();
      return retryWithBackoff(promise, onRetry, maxRetries, retries + 1);
    } else {
      console.warn("Max retries reached. Bubbling the error up");
      throw e;
    }
  }
}
async function pubsub_publish(publisher, data) {
  try {
    const dataBuffer = Buffer.from(JSON.stringify(data));
    await publisher.publishMessage({ data: dataBuffer });
  } catch (error) {
    console.error(`Received error while publishing: ${error.message}`);
    process.exitCode = 1;
  }
}
exports.ProcessProjectIds = async (event, context) => {
  try {
    let url = '';
    const maxMessages = 10;
    const maxWaitTime = 10;
    var pubsubClient = new PubSub();
    const batchPublisher = pubsubClient.topic(process.env.DATASET_TOPICID, {
      batching: {
        maxMessages: maxMessages,
        maxMilliseconds: maxWaitTime * 100,
      },
    });
    const routine_batchPublisher = pubsubClient.topic(process.env.ROUTINE_TOPICID, {
      batching: {
        maxMessages: maxMessages,
        maxMilliseconds: maxWaitTime * 100,
      },
    });
    const datasetdetails_batchPublisher = pubsubClient.topic(process.env.DATASETDETAILS_TOPICID, {
      batching: {
        maxMessages: maxMessages,
        maxMilliseconds: maxWaitTime * 100,
      },
    });
    const message = event.data ? Buffer.from(event.data, 'base64').toString() : 'PROJECTS DATA NOT SUBSCRIBED';
    let projectData = JSON.parse(message);
    url = `${process.env.Google_BaseURL}/projects/${projectData.ProjectId}/datasets?maxResults=5000`;
    const DatasetsList = await retryWithBackoff(
      () => oauthVerification(url),
      () => console.log("onRetry called..."),
      3
    );
    if (DatasetsList && DatasetsList.data && DatasetsList.data.datasets && DatasetsList.data.datasets.length) {
      let datasets = DatasetsList.data.datasets
      for (const dataset of datasets) {
        let data = {
          runId: projectData.runId,
          projectId: projectData.ProjectId,
          datasetId: dataset.datasetReference.datasetId
        }
        await pubsub_publish(batchPublisher, data);
        await pubsub_publish(routine_batchPublisher, data);
        await pubsub_publish(datasetdetails_batchPublisher, data);
      }
    } else {
      console.log("NO DATASETS THERE");
    }
  } catch (err) {
    console.log("error:---------->", err);
  }
};