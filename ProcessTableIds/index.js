const { GoogleAuth } = require('google-auth-library');
var crypto = require('crypto');
const { PubSub } = require('@google-cloud/pubsub');
async function dateTimeFormat(datevalue) {
  return datevalue.getFullYear() + "-" + (datevalue.getMonth() + 1 > 9 ? datevalue.getMonth() + 1 : "0" + (datevalue.getMonth() + 1)) + "-" + ((datevalue.getDate() > 9 ? datevalue.getDate() : "0" + datevalue.getDate())) + " " + (datevalue.getHours() > 9 ? datevalue.getHours() : "0" + (datevalue.getHours())) + ":" + (datevalue.getMinutes() > 9 ? datevalue.getMinutes() : ("0" + datevalue.getMinutes())) + ":" + (datevalue.getSeconds() > 9 ? datevalue.getSeconds() : ("0" + datevalue.getSeconds()))
}
async function oauthVerification(url, method) {
  try {
    const auth = new GoogleAuth({
      scopes: process.env.API_AUTHSCOPE
    });
    const client = await auth.getClient();
    const response = await client.request({ url, method });
    return response;
  } catch (error) {
    console.log("ERROR : --------------------> ", error)
    throw error;
  }
}
function waitFor(milliseconds) {
  return new Promise((resolve) => setTimeout(resolve, milliseconds));
}
async function retryWithBackoff(promise, onRetry, maxRetries, retries = 0) {
  try {
    if (retries > 0) {
      const timeToWait = 2 ** retries * 100;
      console.log(`waiting for ${timeToWait}ms...`);
      await waitFor(timeToWait);
    }
    return await promise();
  } catch (e) {
    if (retries < maxRetries) {
      onRetry();
      return retryWithBackoff(promise, onRetry, maxRetries, retries + 1);
    } else {
      console.warn("Max retries reached. Bubbling the error up");
      throw e;
    }
  }
}
async function pubsub_publish(publisher, data) {
  try {
    const dataBuffer = Buffer.from(JSON.stringify(data));
    await publisher.publishMessage({ data: dataBuffer });
  } catch (error) {
    console.error(`Received error while publishing: ${error.message}`);
    process.exitCode = 1;
  }
}
exports.ProcessTableIds = async (event, context) => {
  try {
    const maxMessages = 10;
    const maxWaitTime = 10;
    var pubsubClient = new PubSub();
    const table_ddl_batchPublisher = pubsubClient.topic(process.env.TABLE_COST_DDL_TOPICID, {
      batching: {
        maxMessages: maxMessages,
        maxMilliseconds: maxWaitTime * 100,
      },
    });
    const message = event.data
      ? Buffer.from(event.data, 'base64').toString()
      : 'NO TABLES AVAILABLE';
    let tableData = JSON.parse(message);
    console.log("tableData:---------->", tableData.tableId);
    url = `${process.env.Google_BaseURL}/projects/${tableData.projectId}/datasets/${tableData.datasetId}/tables/${tableData.tableId}`;
    const tableDetailResponse = await retryWithBackoff(
      () => oauthVerification(url),
      () => console.log("onRetry called..."),
      3
    );
    let tableDetails = tableDetailResponse.data
    /////////GETTING TABLE COST //////////////////////////////////////////////
    let numActiveLogicalBytes = (tableDetails.numActiveLogicalBytes / Math.pow(1024, 3)) * 0.02;
    let numLongTermLogicalBytes = (tableDetails.numLongTermLogicalBytes / Math.pow(1024, 3)) * 0.01;
    let numActivePhysicalBytes = (tableDetails.numActivePhysicalBytes / Math.pow(1024, 3)) * 0.04;
    let numLongTermPhysicalBytes = (tableDetails.numLongTermPhysicalBytes / Math.pow(1024, 3)) * 0.02;
    /////////////////////////////////////////////////////////////////////////////////////////////////
    let tableCreationTime = new Date(parseInt(tableDetails.creationTime))
    tableCreationTime = await dateTimeFormat(tableCreationTime)
    let tableModifiedTime = new Date(parseInt(tableDetails.lastModifiedTime))
    tableModifiedTime = await dateTimeFormat(tableModifiedTime)
    let schemaString = ""
    let tablesColumnsJSON = ''
    let sortedFields = []
    let schemalength = 0
    if (tableDetails.schema && tableDetails.schema.fields) {
      tablesColumnsJSON = JSON.stringify(tableDetails.schema.fields)
      schemalength = tableDetails.schema.fields.length
      let fields = tableDetails.schema.fields
      function GetSortOrder(property) {
        return function (a, b) {
          if (a[property] > b[property]) {
            return 1;
          } else if (a[property] < b[property]) {
            return -1;
          }
          return 0;
        }
      }
      fields.sort(GetSortOrder("name"));
      for (var item in fields) {
        sortedFields.push(fields[item])
      }
      sortedFields.forEach(element => {
        let feildString = element.name + element.type + element.mode
        schemaString = schemaString + feildString
      })
    }
    else {
      tablesColumnsJSON = ""
    }
    let schemaHash = crypto.createHash('md5').update(schemaString).digest('hex');
    let data = {
      runId: tableData.runId,
      projectId: tableDetails.tableReference.projectId,
      datasetId: tableDetails.tableReference.datasetId,
      tableId: tableDetails.tableReference.tableId,
      tableCreationTime: tableCreationTime,
      tableModifiedTime: tableModifiedTime,
      bytesOfTable: tableDetails.numBytes,
      numberOfRows: tableDetails.numRows,
      location: tableDetails.location,
      columnJson: tablesColumnsJSON,
      lengthOfSchema: schemalength,
      schemaHash: schemaHash,
      type: tableDetails.type,
      numActiveLogicalBytes: numActiveLogicalBytes,
      numLongTermLogicalBytes: numLongTermLogicalBytes,
      numActivePhysicalBytes: numActivePhysicalBytes,
      numLongTermPhysicalBytes: numLongTermPhysicalBytes
    }
    await pubsub_publish(table_ddl_batchPublisher, data);
  } catch (err) {
    console.log("error:-------->", err)
  }
};