const { PubSub } = require(`@google-cloud/pubsub`);
const { GoogleAuth } = require('google-auth-library');
async function oauthVerification(url, method) {
  try {
    const auth = new GoogleAuth({
      scopes: process.env.API_AUTHSCOPE
    });
    const client = await auth.getClient();
    const response = await client.request({ url, method });
    return response;
  } catch (error) {
    console.log("ERROR : --------------------> ", error)
    throw error;
  }
}
function waitFor(milliseconds) {
  return new Promise((resolve) => setTimeout(resolve, milliseconds));
}
async function retryWithBackoff(promise, onRetry, maxRetries, retries = 0) {
  try {
    if (retries > 0) {
      const timeToWait = 2 ** retries * 100;
      console.log(`waiting for ${timeToWait}ms...`);
      await waitFor(timeToWait);
    }
    return await promise();
  } catch (e) {
    if (retries < maxRetries) {
      onRetry();
      return retryWithBackoff(promise, onRetry, maxRetries, retries + 1);
    } else {
      console.warn("Max retries reached. Bubbling the error up");
      throw e;
    }
  }
}
async function pubsub_publish(publisher, data) {
  try {
    const dataBuffer = Buffer.from(JSON.stringify(data));
    await publisher.publishMessage({ data: dataBuffer });
  } catch (error) {
    console.error(`Received error while publishing: ${error.message}`);
    process.exitCode = 1;
  }
}
exports.ProcessDatasetIds = async (event, context) => {
  try {
    const maxMessages = 10;
    const maxWaitTime = 10;
    const pubsubClient = new PubSub();
    const batchPublisher = pubsubClient.topic(process.env.TABLE_TOPICID, {
      batching: {
        maxMessages: maxMessages,
        maxMilliseconds: maxWaitTime * 1,
      },
    });
    const message = event.data ? Buffer.from(event.data, 'base64').toString() : 'DATASETS DATA NOT SUBSCRIBED';
    let datasetDetails = JSON.parse(message)
    url = `${process.env.Google_BaseURL}/projects/${datasetDetails.projectId}/datasets/${datasetDetails.datasetId}/tables?maxResults=5000`;
    const tablesListResponse = await retryWithBackoff(
      () => oauthVerification(url),
      () => console.log("onRetry called..."),
      3
    );
    if (tablesListResponse && tablesListResponse.data && tablesListResponse.data.tables && tablesListResponse.data.tables.length) {
      let tableList = tablesListResponse.data.tables
      for (const table of tableList) {
        let id = table.id;
        let tableIddata = id.split(":");
        let tableRefData = tableIddata[1].split(".");
        let tableId = tableRefData[1];
        let data = {
          runId: datasetDetails.runId,
          projectId: datasetDetails.projectId,
          datasetId: datasetDetails.datasetId,
          tableId: tableId
        }
        //////////////////////////////////////////////  #PUBLISHING LIST OF TABLES #  ////////////////////////////////////////////////////////////////////////
        await pubsub_publish(batchPublisher, data)
      }
    }
  } catch (err) {
    console.log("error:----------->--->", err)
  }
};
