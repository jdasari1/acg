const { BigQuery } = require('@google-cloud/bigquery');
async function bigQueryFunction(projectId) {
  return new Promise((resolve, reject) => {
    try {
      var bigquery = new BigQuery({
        projectId: projectId
      })
      bigquery.getDatasets((err, Dataset) => {
        if (Dataset) {
          resolve(bigquery)
        } else {
          reject(err);
        }
      })
    } catch (err) {
      console.log("BIGQUERY CONNECTION ERROR : ######################### ", err)
      reject(err);
    }
  });
}
async function executeBigQuery(projectId, query) {
  try {
    const DBcon = await bigQueryFunction(projectId);
    const data = await DBcon.query(query);
    return data;
  } catch (error) {
    return error;
  }
}
exports.ProcessTableCostLstAccDDL = async (event, context) => {
  try {
    const message = event.data
      ? Buffer.from(event.data, 'base64').toString()
      : 'NO DATA RECIEVED';
    let tableData = JSON.parse(message);
    const bigquery = new BigQuery();
    let tableDetailsListData = [];
    let ddl = "";
    //##############################GETTING LAST ACCESSED TIME OF A PARTICULAR TABLE########################################################
    let lastAccessed_DML = null
    let lastAccessed_SELECT = null
    let dml = lastAccessed_DML == null ? null : `'${lastAccessed_DML}'`;
    let la_select = lastAccessed_SELECT == null ? null : `'${lastAccessed_SELECT}'`;
    //##############GETTING DDL VALUE#######################################//
    query = `select table_catalog,table_schema,table_name, ddl from ${tableData.datasetId}.INFORMATION_SCHEMA.TABLES where table_name='${tableData.tableId}'`;
    let data = await executeBigQuery(tableData.projectId, query).then((result) => {
      return result;
    })
    if (data && data[0] && data[0][0] && data[0][0].ddl) {
      ddl = data[0][0].ddl.replace(/\n/g, '');
    }
    else {
      ddl = null;
    }
    // ####################### INSERTING DATA INTO THE DatasetTables ############################################//
    let tableDetailsList = {
      RunId: tableData.runId,
      ProjectId: tableData.projectId,
      DatasetId: tableData.datasetId,
      TableId: tableData.tableId,
      CreatedDatetime: tableData.tableCreationTime,
      LastModifiedDatetime: tableData.tableModifiedTime,
      TableSizeInBytes: parseInt(tableData.bytesOfTable),
      NumberofRows: parseInt(tableData.numberOfRows),
      Location: tableData.location,
      ColumnsJson: tableData.columnJson,
      NumberofColumns: parseInt(tableData.lengthOfSchema),
      TableSchemaHash: tableData.schemaHash,
      LastAccessedTimestamp_SELECTOnly: dml,
      LastAccessedTimestamp_DMLOnly: la_select,
      Type: tableData.type,
      DDLStatement: ddl,
      ActiveLogicalCost: tableData.numActiveLogicalBytes,
      LongTermLogicalCost: tableData.numLongTermLogicalBytes,
      ActivePhysicalCost: tableData.numActivePhysicalBytes,
      LongTermPhysicalCost: tableData.numLongTermPhysicalBytes
    }
    tableDetailsListData.push(tableDetailsList)
    await bigquery
      .dataset("MetricsExtraction")
      .table("DatasetTables")
      .insert(tableDetailsListData);
    console.log(`Inserted ${tableDetailsListData.length} rows`);
  } catch (err) {
    console.log("error:----------->--->", err)
  }
};