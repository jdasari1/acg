const { BigQuery } = require('@google-cloud/bigquery');
exports.ProcessQueryLogInsert = async (event, context) => {
    try {
        const bigquery = new BigQuery();
        const message = event.data ? Buffer.from(event.data, 'base64').toString() : 'DATA NOT RECIEVEED';
        let queryLogData = JSON.parse(message);
        let queryLogInsertionData = []
        queryLogInsertionData.push(queryLogData)
        // Insert data into a table
        await bigquery
            .dataset("MetricsExtraction")
            .table("QueryLogs")
            .insert(queryLogInsertionData);
        console.log(`Inserted ${queryLogInsertionData.length} rows`);
    } catch (err) {
        console.log("error:---->--->", err)
    }
};