const { BigQuery } = require('@google-cloud/bigquery');
const { GoogleAuth } = require('google-auth-library');
const { PubSub } = require('@google-cloud/pubsub');
const dateTimeFormat = async (datevalue) => {
  return datevalue.getFullYear() + "-" + (datevalue.getMonth() + 1 > 9 ? datevalue.getMonth() + 1 : "0" + (datevalue.getMonth() + 1)) + "-" + ((datevalue.getDate() > 9 ? datevalue.getDate() : "0" + datevalue.getDate())) + " " + (datevalue.getHours() > 9 ? datevalue.getHours() : "0" + (datevalue.getHours())) + ":" + (datevalue.getMinutes() > 9 ? datevalue.getMinutes() : ("0" + datevalue.getMinutes())) + ":" + (datevalue.getSeconds() > 9 ? datevalue.getSeconds() : ("0" + datevalue.getSeconds()))
}
const bigQueryFunction = async (projectId) => {
  return new Promise((resolve, reject) => {
    try {
      var bigquery = new BigQuery({
        projectId: projectId
      })
      bigquery.getDatasets((err, Dataset) => {
        if (Dataset) {
          resolve(bigquery)
        }
        else {
          reject(err);
        }
      })
    } catch (err) {
      console.log("BIGQUERY CONNECTION ERROR : ############################### ", err)
      reject(err);
    }
  });
}
const executeBigQuery = async (projectId, query) => {
  try {
    await bigQueryFunction(projectId).then(async (DBcon) => {
      data = await DBcon.query(query)
    })
    return data
  } catch (error) {
    return error
  }
}
const oauthVerification = async (url, method) => {
  try {
    const auth = new GoogleAuth({
      scopes: process.env.API_AUTHSCOPE
    });
    var client = await auth.getClient();
    const response = await client.request({ url, method });
    return response;
  } catch (error) {
    return error;
  }
}
function waitFor(milliseconds) {
  return new Promise((resolve) => setTimeout(resolve, milliseconds));
}
async function retryWithBackoff(promise, onRetry, maxRetries, retries = 0) {
  try {
    if (retries > 0) {
      const timeToWait = 2 ** retries * 100;
      console.log(`waiting for ${timeToWait}ms...`);
      await waitFor(timeToWait);
    }
    return await promise();
  } catch (e) {
    if (retries < maxRetries) {
      onRetry();
      return retryWithBackoff(promise, onRetry, maxRetries, retries + 1);
    } else {
      console.warn("Max retries reached. Bubbling the error up");
      throw e;
    }
  }
}
exports.ProcessJobIds = async (event, context) => {
  try {
    let query = '';
    const message = event.data ? Buffer.from(event.data, 'base64').toString() : 'NO PROJECTS ARE AVAILABLE';
    console.log("message:----------->", message);
    let jobData = JSON.parse(message);
    //##############################GETTING LIST OF JOBS#########################################################
    url = `${process.env.Google_BaseURL}/projects/${jobData.ProjectId}/jobs?maxResults=5000`;
    let jobsResponse = [];
    const jobsList = await retryWithBackoff(
      () => oauthVerification(url),
      () => console.log("onRetry called..."),
      3
    );
    jobsResponse.push(jobsList);
    jobsResponse.map(async function (result) {
      result.data.jobs.map(async (item) => {
        //###################################OTHER DETAILS FROM JOBS ################################################################################
        url = `${process.env.Google_BaseURL}/projects/${item.jobReference.projectId}/jobs/${item.jobReference.jobId}`;
        const JobsData = await retryWithBackoff(
          () => oauthVerification(url),
          () => console.log("onRetry called..."),
          3
        );
        console.log("jobsResponse: ", JobsData.data.statistics.query)
        // //##############################INSERTING LIST OF JOBS IN JOBS TABLE MYSQL#########################################################
        let ref = JobsData.data.statistics.query.referencedTables[0]
        let jobCreationTime = JobsData.data.statistics.creationTime;
        jobCreationTime = new Date(parseInt(jobCreationTime))
        jobCreationTime = await dateTimeFormat(jobCreationTime)
        let jobStartTime = JobsData.data.statistics.startTime;
        jobStartTime = new Date(parseInt(jobStartTime))
        jobStartTime = await dateTimeFormat(jobStartTime)
        let jobEndTime = JobsData.data.statistics.endTime;
        jobEndTime = new Date(parseInt(jobEndTime))
        jobEndTime = await dateTimeFormat(jobEndTime)
        query = `INSERT INTO bqaaf-development.MetricsExtraction.Jobs (RunId,ProjectId,JobId,DatasetId,TableId,JobType,CreationTimestamp,StartTimestamp,EndTimestamp,TotalBytesProcessed,CostSpend,ExecutedBy,MemoryUsedInBytes)VALUES ('${jobData.runId}','${JobsData.data.jobReference.projectId}','${JobsData.data.jobReference.jobId}','${ref.datasetId}','${ref.tableId}','${JobsData.data.configuration.jobType}','${jobCreationTime}','${jobStartTime}','${jobEndTime}',${parseInt(JobsData.data.statistics.totalBytesProcessed)},${parseInt(JobsData.data.statistics.query.totalBytesBilled)},'${JobsData.data.user_email}','${JobsData.data.statistics.query.estimatedBytesProcessed}')`
        await executeBigQuery(jobData.ProjectId, query).then((result) => {
          console.log("RUNID : ", jobData.runId, "  INSERTING LIST OF JOBS IN JOBS TABLE RESPONSE :-------------------------------->", result)
        })
      })
    })
  } catch (err) {
    console.log("error:---->--->", err)
  }
};
