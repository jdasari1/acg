const { GoogleAuth } = require('google-auth-library');
const { PubSub } = require('@google-cloud/pubsub');
async function dateTimeFormat(datevalue) {
  return datevalue.getFullYear() + "-" + (datevalue.getMonth() + 1 > 9 ? datevalue.getMonth() + 1 : "0" + (datevalue.getMonth() + 1)) + "-" + ((datevalue.getDate() > 9 ? datevalue.getDate() : "0" + datevalue.getDate())) + " " + (datevalue.getHours() > 9 ? datevalue.getHours() : "0" + (datevalue.getHours())) + ":" + (datevalue.getMinutes() > 9 ? datevalue.getMinutes() : ("0" + datevalue.getMinutes())) + ":" + (datevalue.getSeconds() > 9 ? datevalue.getSeconds() : ("0" + datevalue.getSeconds()))
}
async function oauthVerification(url, method) {
  try {
    const auth = new GoogleAuth({
      scopes: process.env.API_AUTHSCOPE
    });
    const client = await auth.getClient();
    const response = await client.request({ url, method });
    return response;
  } catch (error) {
    console.log("ERROR : --------------------> ", error)
    throw error;
  }
}
function waitFor(milliseconds) {
  return new Promise((resolve) => setTimeout(resolve, milliseconds));
}
async function retryWithBackoff(promise, onRetry, maxRetries, retries = 0) {
  try {
    if (retries > 0) {
      const timeToWait = 2 ** retries * 100;
      console.log(`waiting for ${timeToWait}ms...`);
      await waitFor(timeToWait);
    }
    return await promise();
  } catch (e) {
    if (retries < maxRetries) {
      onRetry();
      return retryWithBackoff(promise, onRetry, maxRetries, retries + 1);
    } else {
      console.warn("Max retries reached. Bubbling the error up");
      throw e;
    }
  }
}
async function pubsub_publish(publisher, data) {
  try {
    const dataBuffer = Buffer.from(JSON.stringify(data));
    await publisher.publishMessage({ data: dataBuffer });
  } catch (error) {
    console.error(`Received error while publishing: ${error.message}`);
    process.exitCode = 1;
  }
}
exports.ProcessStorageBuckets = async (event, context) => {
  try {
    const message = event.data ? Buffer.from(event.data, 'base64').toString() : 'PROJECT ID NOT SUBSCRIBED';
    let projectData = JSON.parse(message);
    console.log("projectData:-------------->", projectData)
    const maxMessages = 10;
    const maxWaitTime = 10;
    var pubsubClient = new PubSub();
    const processStorageBucketDetail_batchPublisher = pubsubClient.topic(process.env.STORAGE_BUCKET_DETAIL_TOPICID, {
      batching: {
        maxMessages: maxMessages,
        maxMilliseconds: maxWaitTime * 100,
      },
    });
    const processStorageBucketName_batchPublisher = pubsubClient.topic(process.env.STORAGE_BUCKET_NAME_TOPICID, {
      batching: {
        maxMessages: maxMessages,
        maxMilliseconds: maxWaitTime * 100,
      },
    });
    let url = `${process.env.STORAGE_GOOGLE_BASEURL}project=${projectData.ProjectId}&maxResults=5000`;
    console.log("url:-------------------->", url);
    const buckets = await retryWithBackoff(
      () => oauthVerification(url),
      () => console.log("onRetry called..."),
      3
    );
    if (buckets.data && buckets.data.items) {
      for (const bucket of (buckets.data.items)) {
        try {
          let bucketNameDetails = {
            RunId: projectData.runId,
            ProjectId: projectData.projectId,
            BucketName: bucket.name
          }
          let bucketsCreationTime = bucket.timeCreated;
          bucketsCreationTime = new Date(bucketsCreationTime);
          bucketsCreationTime = await dateTimeFormat(bucketsCreationTime);
          let bucketsupdatedTime = bucket.updated;
          bucketsupdatedTime = new Date(bucketsupdatedTime);
          bucketsupdatedTime = await dateTimeFormat(bucketsupdatedTime);
          //#####################getting cost#####################////////
          let storageBucketData = {
            RunId: projectData.runId,
            ProjectId: projectData.projectId,
            BucketId: bucket.id,
            CreationTimestamp: bucketsCreationTime,
            LastModifiedTimestamp: bucketsupdatedTime,
            SizeInBytes: 0,
            StorageClass: bucket.storageClass,
            StorageCost: 0
          }
          await pubsub_publish(processStorageBucketDetail_batchPublisher, storageBucketData);
          await pubsub_publish(processStorageBucketName_batchPublisher, bucketNameDetails);
        } catch (err) {
          console.log("error:--------->", err)
        }
      }
    }
  } catch (err) {
    console.log("error:------------>--->", err)
  }
};
