const { BigQuery } = require('@google-cloud/bigquery');
exports.ProcessExcessPermissionsDetails = async (event, context) => {
    try {
        const bigquery = new BigQuery();
        const message = event.data ? Buffer.from(event.data, 'base64').toString() : 'DATA NOT RECIEVEED';
        let excessPermissionData = JSON.parse(message);
        console.log("excessPermissionData---------->", excessPermissionData);
        // Insert data into a table
        await bigquery
            .dataset("MetricsExtraction")
            .table("ExcessPermissions")
            .insert(excessPermissionData);
        console.log(`Inserted ${excessPermissionData.length} rows`);
    } catch (err) {
        console.log("error:---->--->", err)
    }
};