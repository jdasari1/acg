var { BigQuery } = require('@google-cloud/bigquery');
exports.ProcessBucketSize = async (event, context) => {
    try {
        const bigquery = new BigQuery();
        const message = event.data ? Buffer.from(event.data, 'base64').toString() : 'BUCKET DETAILS NOT SUBSCRIBED';
        let bucketSizeData = JSON.parse(message);
        let bucketSizeDetails = []
        bucketSizeDetails.push(bucketSizeData)
        await bigquery
            .dataset("MetricsExtraction")
            .table("BucketSizeDetails")
            .insert(bucketSizeDetails);
        console.log(`Inserted ${bucketSizeDetails.length} rows`)
        console.log("bucketSizeDetails:-------------->", bucketSizeDetails);
    } catch (err) {
        console.log("error:------>", err)
    }

};
